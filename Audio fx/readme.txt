FX2 rev .08 January 2000
========================

Introduction
============

FX2 is a sound effects generator, sound processor and virtual 
synthesiser for Windows 98. This README file assumes you are 
reasonably familiar with DOS and Windows.


Disclaimer
==========

This program and its documentation are provided "as is" without warranty
of any kind. The author makes no guarantee of correctness, accuracy,
reliability, safety or performance. You alone are responsible for
determining if this program is safe for use in your environment.


Terms and conditions of use
===========================

FX2 is not free. This program and its documentation are copyright (c) Nick 
Jones 1996 - 2000. This product is shareware, you may evaluate it for 
a period of up to 30 days. If, after this period, you wish to continue to 
use FX2 you must pay a license fee. Licensing arrangements are explained 
in the program's help file.


Warnings
========

FX generates both WAV files and direct output for your sound card. It's
possible to use FX to generate frequencies and power levels that can't be
found in normal music. It's therefore theoretically possible to overload
something connected to the sound card output (e.g. speakers, an external
amplifier or your ears!). I must stress that I haven't been able to do this
on my system but I can't discount the possibility that someone, somewhere
could create a layout which would do so. When you're using FX2 things to do
include (but aren't limited to):

a) Make sure that you have output volumes set to a low level - especially
   when first playing new sounds.

b) Don't use headphones.

c) Disconnect external kit such as amplifiers.


Hardware and software requirements
==================================

FX2 will run on any PC running Windows 95 or Windows 98 with a minimum of
10.0Mb free disk space, a 16 bit sound card and DirectX V6 or later. FX2 
can be very processor and memory hungry so faster and better configured 
PCs are better. The help file discusses hardware requirements and performance
issues in more detail. A 16 bit sound card is essential, full duplex 
capability is required for some FX2 features.


Installation
============

1. Double click the self-extracting ZIP file fx2.exe, when asked to 
   specify the directory name into which you'd like FX2 installed enter 
   any convenient name. This will unpack FX2 into the directory you
   specify.

2. Run the program, click the help button and read the help file to 
   get instructions on using FX2. This version of FX2 includes a 
   detailed tutorial with over 60 examples.


For users of earlier versions
=============================

The help file contains a detailed list of updates and changes. Highlights
include four new tools, various bug fixes, improved handling of very large
sounds.


Comments, suggestions and bugs
==============================

If you have any comments, suggestions, find any bugs or want to purchase
a license for FX2 please send me an email: nick@njones.demon.co.uk


